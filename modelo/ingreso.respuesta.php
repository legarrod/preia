<?php 

require_once "Conexion.php";

class ModeloRespuesta{

    static public function mdlRespuesta($tabla, $datos){

        $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla
        (misrespuestasrecibidas)
      VALUES
        (:misrespuestasrecibidas)");

        $stmt->bindParam(":misrespuestasrecibidas", $datos["respuesta"], PDO::PARAM_STR);
     

        if($stmt->execute()){
            return "ok";
        }else{
            return print_r(Conexion::conectar()->errorInfo());
        }

        $stmt->close();
        $stmt = null;

    }

    
}