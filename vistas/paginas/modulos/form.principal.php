
<?php 
	
	require_once "modelo/Conexion.php";
  $conexion=new Conexion();
 $con =$conexion -> conectar();

 ?>



 <div class="row">
 <div class="col-12 col-md-6 col-lg-6">  
    <img class="img-fluid" src="img/1.webp">
    <form method="post" action="" class="">
    <h3 class="col-12 col-md-12 col-lg-12 text-center"> ¿Que le preguntarias a un sistema de inteligencia artificial? </h3>
        
          <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
            <label class="col-form-label" for="inputDefault"></label>
            <input type="text" class="form-control" placeholder="Tu respuesta aquí" id="inputDefault" name="preiaRespuesta" require>
          </div>
          <?php 
          
          $registroRespuesta = new ControladorRespuestas();
          $registroRespuesta -> ctrRegistroRespuestas();

        
        ?>
          <div class="container text-right mb-3">
          <button type="submit" class="btn btn-primary mb-5 ">Guardar</button>
          </div>
      
    </form>
  </div>
  
   
  <div class="container home col-12 col-sm-12 col-md-6 col-lg-6 ">       
    <h3 class="col-12 col-sm-12 col-md-12 col-lg-12 text-center">Últimas 10 preguntas</h3>  
    <div class="col-12 col-sm-12 col-md-12 col-lg-12">      
        <table id="data_table" class="table table-striped table-bordered table-sm">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Preguntas</th>
                   
                </tr>
            </thead>
            <tbody>
              <?php 
                  
                    $sql="SELECT idrespuestas,misrespuestasrecibidas
                      from respuestas LIMIT 10";
                
          
                  
                  //$con=mysqli_query($sql);
                  $result= $con->prepare($sql);
                  $result->execute();
                  $data= $result->fetchALL(PDO::FETCH_ASSOC);
                  
                  foreach ($data as $mostrar) {
        
                
              ?>
              <tr>
                    <th><?php echo $mostrar['idrespuestas']; ?></th>
                    <th><?php echo $mostrar['misrespuestasrecibidas']; ?></th>

              </tr>
                  <?php }?>
                  </tbody>
        </table> 
      </div>
  </div>   
</div>  
