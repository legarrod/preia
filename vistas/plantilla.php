<?php 

	$ruta = ControladorRuta::ctrRuta();

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Investigación cientifica</title>

	<base href="vistas/">

	<link rel="icon" href="img/icono.png">
	

	<!--=====================================
	VÍNCULOS CSS
	======================================-->

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="css/bootstrap.min.css">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

	<!-- Fuente Open Sans -->
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed|Roboto:100|Grand+Hotel" rel="stylesheet">
	
	<!-- https://sweetalert2.github.io/ -->
	<script src="js/plugins/sweetalert2.all.js"></script>
	<script src="js/plugins/validacion.js"></script>
	

</head>

<body>

<?php 
		if(isset($_GET["pagina"])){

			if(
				$_GET["pagina"] == "respuestas"){

					include "paginas/".$_GET["pagina"].".php";

				}

		}else{

			include "paginas/inicio.php";

		}

        
		

        ?>

</body>

</html>